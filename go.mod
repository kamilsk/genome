module github.com/kamilsk/genome

go 1.11

require (
	github.com/stretchr/testify v1.5.1
	go.octolab.org v0.0.22
	go.octolab.org/toolkit/cli v0.0.9
)
